package ub.Commands;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class DisableTeleportCommand implements CommandExecutor{
	public UltraBasics plugin;
	public DisableTeleportCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length != 0){
			plugin.noRightArgs(p, "/DisableTeleport | /DTp");
			return true;
		}
		if(plugin.disabletp.contains(p)){
			p.sendMessage(plugin.prefix + ChatColor.RED + "Teleporting is already disabled!");
			return true;
		}
		if(plugin.m.checkPermission(p, "ultrabasics.disableteleport", true)){
			plugin.disabletp.add(p);
			p.sendMessage(plugin.prefix + ChatColor.AQUA + "You disabled teleporting to you!");
		}
		return false;
	}

}
