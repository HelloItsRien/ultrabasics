package ub.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import ub.Main.UltraBasics;

public class RepairCommand implements CommandExecutor{
	public UltraBasics plugin;
	public RepairCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length != 0 && args.length != 1){
			plugin.noRightArgs(p, "/Repair");
			return true;
		}
		if(args.length == 0){
			ItemStack h = p.getItemInHand();
			if(h == null){
				p.sendMessage(plugin.prefix + ChatColor.RED + "You don't have anything in your hand!");
				return true;
			}
			try{
				h.setDurability((short)0);
				p.sendMessage(plugin.prefix + ChatColor.GREEN + "The item in your hand is repaired!");
			}
			catch(Exception e){
				e.printStackTrace();
				p.sendMessage(plugin.prefix + ChatColor.RED + "The item couldn't repair!");
			}

		}
		if(args.length == 1){
			Player t = Bukkit.getPlayer(args[0]);
			if(t == null || !t.isOnline()){
				plugin.playerNotOnline(p, t);
				return true;
			}
			ItemStack h = t.getItemInHand();
			if(h == null){
				p.sendMessage(plugin.prefix + ChatColor.RED + t.getDisplayName() + " don't have anything in your hand!");
				return true;
			}
			try{
				h.setDurability((short)0);
				p.sendMessage(plugin.prefix + ChatColor.GREEN + "The item in " + t.getDisplayName() + "s hand is repaired!");
			}
			catch(Exception e){
				e.printStackTrace();
				p.sendMessage(plugin.prefix + ChatColor.RED + "ERROR: The item couldn't repair!");
			}
		}
		return false;
	}

}
