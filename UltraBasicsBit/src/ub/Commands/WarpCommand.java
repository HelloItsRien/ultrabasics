package ub.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class WarpCommand implements CommandExecutor{
	public UltraBasics plugin;
	public WarpCommand(UltraBasics plugin){
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length != 1){
			plugin.noRightArgs(p, "/warp <WarpName>");
			return true;
		}
		if(!plugin.wm.isExistingWarp(args[0])){
			p.sendMessage(plugin.prefix + ChatColor.RED + "That warp doesn't exist!");
			return true;
		}
		if(plugin.wm.getWarp(args[0], p, false) == null){
			p.sendMessage(plugin.prefix + ChatColor.RED + "The warp can't be null!");
			return true;
		}
		p.teleport(plugin.wm.getWarp(args[0], p, true));
		return false;
	}

}
