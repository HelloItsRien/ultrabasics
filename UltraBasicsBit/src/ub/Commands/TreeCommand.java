package ub.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class TreeCommand implements CommandExecutor{
	public UltraBasics plugin;
	public TreeCommand(UltraBasics plugin){
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length != 1 || args.length != 2){
			plugin.noRightArgs(p, "/Tree <Type> <Size>");
			return true;
		}
		
		return false;
	}

}
