package ub.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class GamemodeCommand implements CommandExecutor {
	public UltraBasics plugin;
	public GamemodeCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player)sender;

		if(!(sender instanceof Player)){
			return true;
		}
		if (args.length == 0) {
			if(plugin.m.checkPermission(p, "ultrabasics.gamemode", true)){
				sender.sendMessage(ChatColor.YELLOW + "Change player gamemode");
				sender.sendMessage(ChatColor.YELLOW + "Right syntax: /gm <creative:survival:adventure> <player>");
			}
		}
		else if (args.length == 1) {
			if(plugin.m.checkPermission(p, "ultrabasics.gamemode", true)){
				if ((args[0].contains("creat")) || (args[0].equalsIgnoreCase("1"))) {
					p.setGameMode(GameMode.CREATIVE);
					p.sendMessage(ChatColor.YELLOW + "You have given yourself Creative!");
				}
				if ((args[0].contains("survi")) || (args[0].equalsIgnoreCase("0"))) {
					p.setGameMode(GameMode.SURVIVAL);
					p.sendMessage(ChatColor.YELLOW + "You have given yourself Survival!");
				}
				if ((args[0].contains("advent")) || (args[0].equalsIgnoreCase("2"))) {
					p.setGameMode(GameMode.ADVENTURE);
					p.sendMessage(ChatColor.YELLOW + "You have given yourself Adventure!");
				}
			}
		}
		else if (args.length == 2) {
			if (!sender.hasPermission("ultrabasics.gamemode.other")) {
				p.sendMessage(ChatColor.YELLOW + "You don't have the needed permission");
				return true;
			}
			Player target = Bukkit.getServer().getPlayer(args[1]);
			if (target == null) {
				p.sendMessage(ChatColor.YELLOW + "Invalid player");
			}
			if ((args[0].contains("creat")) || (args[0].equalsIgnoreCase("1"))) {
				target.setGameMode(GameMode.CREATIVE);
				target.sendMessage(p.getPlayer().getName() + " has given you creative!");
				p.sendMessage(ChatColor.YELLOW + "You gave " + target.getName() + " creative!");
			}
			else if ((args[0].contains("survi")) || (args[0].equalsIgnoreCase("0"))){
				target.setGameMode(GameMode.SURVIVAL);
				target.sendMessage(p.getPlayer().getName() + " has given you survival!");
				p.sendMessage(ChatColor.YELLOW + "You gave " + target.getName() + " survival!");
			}
			else if ((args[0].contains("advent")) || (args[0].equalsIgnoreCase("2"))){
				target.setGameMode(GameMode.ADVENTURE);
				target.sendMessage(p.getPlayer().getName() + " has given you adventure!");
				p.sendMessage(ChatColor.YELLOW + "You gave " + target.getName() + " adventure!");
			} else {
				p.sendMessage(ChatColor.YELLOW + "Well, this isn't anything. Fail");
			}
		} else {
			plugin.noRightArgs(p, "/gm <creative:survival:adventure> <player>");
		}
		return false;	
	}

}
