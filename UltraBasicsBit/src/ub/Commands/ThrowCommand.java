package ub.Commands;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import ub.Main.UltraBasics;

public class ThrowCommand implements CommandExecutor{
	public UltraBasics plugin;
	public ThrowCommand(UltraBasics plugin){
		this.plugin = plugin;
	}
	int c = 30;
	public BukkitTask task;

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length != 1){
			plugin.noRightArgs(p, "/throw <Player>");
			return true;
		}
		if(plugin.m.checkPermission(p, "ultrabasics.throw", true)){
			Player t = Bukkit.getPlayer(args[0]);
			if(t == null | !t.isOnline()){
				plugin.playerNotOnline(p, t);
				return true;
			}
			Random r = new Random();
			int i = r.nextInt(4) + 1;
			t.sendMessage(plugin.prefix + ChatColor.AQUA + "Wooooosh");
			if(i == 1){
				t.setVelocity(new Vector(1.5, 5 ,0));
				return true;
			}
			if(i == 2){
				t.setVelocity(new Vector(0, 5 ,1.5));
				return true;
			}
			if(i == 3){
				t.setVelocity(new Vector(0, 5, 0));
				return true;
			}
			if(i == 4){
				t.setVelocity(new Vector(2.5, 5, 2.5));
				return true;
			}
		}
		return false;
	}

}
