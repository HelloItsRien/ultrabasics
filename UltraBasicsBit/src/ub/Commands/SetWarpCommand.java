package ub.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class SetWarpCommand implements CommandExecutor{
	public UltraBasics plugin;
	public SetWarpCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length != 1){
			plugin.noRightArgs(p, "/Setwarp <WarpName>");
		}
		if(plugin.m.checkPermission(p, "ultrabasics.setwarp", true)){
			plugin.wm.createWarp(args[0], p.getLocation(), p);
		}
		return false;
	}

}
