package ub.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class FreezeCommand implements CommandExecutor{
	public UltraBasics plugin;
	public FreezeCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length == 0){
			plugin.noRightArgs(p, "/Freeze <Player>");
		}
		else if(args.length == 1){
			Player t = Bukkit.getPlayer(args[0]);
			if(plugin.m.checkPermission(p, "ultrabasics.freeze", true)){
				if(t != null){
					if(!t.isOp() || !t.hasPermission("ultrabasics.admin")){
						if(!plugin.frozen.contains(t)){
							plugin.frozen.add(t);
							t.sendMessage(plugin.prefix + ChatColor.RED + "You're frozen!");
							p.sendMessage(plugin.prefix + ChatColor.GREEN + "You've frozen " + t.getDisplayName() + "!");
						} else {
							p.sendMessage(plugin.prefix + ChatColor.RED + "That player is already frozen!");
						}
					} else {
						p.sendMessage(plugin.prefix + ChatColor.RED + "You're not allowed to freeze that player!");
					}
				} else {
					plugin.playerNotOnline(p, t);
				}
			}
		} else {
			plugin.noRightArgs(p, "/Freeze <Player>");
		}
		return false;
	}

}
