package ub.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class NickCommand implements CommandExecutor{
	public UltraBasics plugin;
	public NickCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length == 0){

		}
		if(args.length == 1){
			if(plugin.m.checkPermission(p, "ultrabasics.nick", true)){
				String n = args[0];
				if(n.toLowerCase().equalsIgnoreCase("clear")){
					p.setDisplayName(p.getName());
					return true;
				}
				if(plugin.m.checkPermission(p, "ultrabasics.nick.color", false)){
					n = n.replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
				} else {
					n = n.replaceAll("(&([a-fk-or0-9]))", "");
				}
				p.setDisplayName(n);
				p.sendMessage(plugin.prefix + ChatColor.GREEN + "Your displayname is changed to " + n);
			}
		}
		if(args.length == 2){
			Player t = Bukkit.getPlayer(args[0]);
			String n = args[1];
			if(t != null){
				if(plugin.m.checkPermission(p, "ultrabasics.nick.other", true)){
					if(n.toLowerCase().equalsIgnoreCase("clear")){
						t.setDisplayName(t.getName());
						return true;
					}
					if(plugin.m.checkPermission(p, "ultrabasics.nick.other.color", false)){
						n = n.replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
					} else {
						n = n.replaceAll("(&([a-fk-or0-9]))", "");
					}
					t.setDisplayName(n);
					t.sendMessage(plugin.prefix + ChatColor.GREEN + "Your displayname is changed to " + n);
				}
			} else {
				plugin.playerNotOnline(p, t);
			}
		}
		return false;
	}

}
