package ub.Commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import ub.Main.UltraBasics;

public class HatCommand implements CommandExecutor {
	public UltraBasics plugin;
	public HatCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)){
			return true;
		} else {
			Player p = (Player) sender;
			if(args.length == 0){
				if(plugin.m.checkPermission(p, "ultrabasics.hat", true)){                                 
					if (p.getItemInHand().getType() != Material.AIR){
						ItemStack inHanditem = p.getItemInHand();
						ItemStack getArmor = p.getInventory().getHelmet();
						p.getInventory().setHelmet(inHanditem);
						if(p.getInventory().getHelmet().getType() != Material.AIR){
							p.getInventory().setItemInHand(getArmor);
						} else {
							p.getInventory().setHelmet(null);
						}
						p.sendMessage(plugin.prefix + ChatColor.GREEN + "Enjoy your new hat!");
					} else {
						p.sendMessage(plugin.prefix + ChatColor.RED + "You haven't selected anything!");
					}
				}
			}
		}
		return false;
	}
}