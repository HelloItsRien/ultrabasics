package ub.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class SurvivalCommand implements CommandExecutor{
	public UltraBasics plugin;
	public SurvivalCommand(UltraBasics plugin){
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length == 0){
			if(plugin.m.checkPermission(p, "ultrabasics.gamemode", true)){
				if(p.getGameMode() != GameMode.SURVIVAL){
					p.setGameMode(GameMode.SURVIVAL);
					p.sendMessage(plugin.prefix + ChatColor.GREEN + "Your gamemode has been changed to survival!");
				} else {
					p.sendMessage(plugin.prefix + ChatColor.RED + "Your gamemode is already survival!");
				}
			}
		}
		if(args.length == 1){
			if(plugin.m.checkPermission(p, "ultrabasics.gamemode.other", true)){
				Player t = Bukkit.getPlayer(args[0]);
				if(t != null){
					if(t.getGameMode() != GameMode.SURVIVAL){
						t.setGameMode(GameMode.SURVIVAL);
						t.sendMessage(plugin.prefix + ChatColor.GREEN + "Your gamemode has been changed into survival!");
						p.sendMessage(plugin.prefix + ChatColor.GREEN + "You've changed " + t.getDisplayName() + " into survival!");
					} else {
						p.sendMessage(plugin.prefix + ChatColor.RED + t.getDisplayName() + "s gamemode is already survival!");
					}
				} else {
					plugin.playerNotOnline(p, t);
				}
			}
		}
		return false;
	}

}