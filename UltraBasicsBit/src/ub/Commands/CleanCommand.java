package ub.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class CleanCommand implements CommandExecutor{
	public UltraBasics plugin;
	public CleanCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length == 0){
			if(plugin.m.checkPermission(p, "ultrabasics.clean", true)){
				p.getActivePotionEffects().clear();
				p.setWalkSpeed(1.0F);
				p.setFlySpeed(1.0F);
				p.setHealth(20.0D);
				p.setFoodLevel(20);
				p.sendMessage(plugin.prefix + ChatColor.GOLD + "You're now fully clean!");
			}
		}
		if(args.length == 1){
			Player t = Bukkit.getPlayer(args[0]);
			if(plugin.m.checkPermission(p, "ultrabasics.clean", true)){
				if(t != null){
					t.getActivePotionEffects().clear();
					t.setWalkSpeed(1.0F);
					t.setFlySpeed(1.0F);
					t.setHealth(20.0D);
					t.setFoodLevel(20);
					t.sendMessage(plugin.prefix + ChatColor.GOLD + "You're now fully clean!");
					p.sendMessage(plugin.prefix + ChatColor.GREEN + "You've cleaned " + t.getDisplayName() + "!");
				} else {
					plugin.playerNotOnline(p, t);
				}
			}
		}
		return false;
	}

}
