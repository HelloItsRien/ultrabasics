package ub.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_7_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class HealCommand implements CommandExecutor{
	public UltraBasics plugin;
	public HealCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		} else {
			Player p = (Player) sender;
			if(args.length == 0){
				if(plugin.m.checkPermission(p, "ultrabasics.heal", true)){
					int h = (int) ((CraftPlayer)p).getHandle().getHealth();
					int m = (int) ((CraftPlayer)p).getHandle().getMaxHealth();
					if(h <= m){
						try{
							p.setHealth(20.0D);
							p.sendMessage(plugin.prefix + ChatColor.GREEN + "You're healed!");
						}
						catch(Exception e){
							e.printStackTrace();
							p.sendMessage(plugin.prefix + ChatColor.RED + "You could not be healed!");
						}
					} else {
						p.sendMessage(plugin.prefix + ChatColor.RED + "Your healthbar is already full!");
					}
				}
			}
			else if(args.length == 1){
				if(plugin.m.checkPermission(p, "ultrabasics.heal.other", true)){
					Player t = Bukkit.getPlayer(args[0]);
					if(t != null){
						t.setHealth(20.0D);
						t.sendMessage(plugin.prefix + ChatColor.GREEN + "You're healed by " + ChatColor.RESET + p.getDisplayName() + ChatColor.GREEN + "!");
						p.sendMessage(plugin.prefix + ChatColor.GREEN + "You've healed " + ChatColor.RESET + t.getDisplayName() + ChatColor.GREEN + " successful!");
					} else {
						plugin.playerNotOnline(p, t);
					}
				}
			} else {
				plugin.noRightArgs(p, "/heal | /heal <Player>");
			}
		}
		return false;
	}

}