package ub.Commands;


import net.minecraft.util.org.apache.commons.lang3.StringUtils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class MessageCommand implements CommandExecutor {
	public UltraBasics plugin;
	public MessageCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	String prefix = ChatColor.GRAY + "[" + ChatColor.AQUA + "UltraBasics" + ChatColor.GRAY + "] ";

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		Player p = (Player) sender;

		if (plugin.m.checkPermission(p, "ultrabasics.msg", true)) {
			if (args.length == 0) {
				p.sendMessage(this.prefix + "Right usage: /Msg <Player> <message>");
			}
			if (args.length >= 2){
				Player target = Bukkit.getServer().getPlayer(args[0]);
				String message = StringUtils.join(args, ' ', 1, args.length);
				if (target == null) {
					p.sendMessage(this.prefix + "That player doesnt�t exists or isn�t online!");
				}
				else if (target != null) {
					target.sendMessage(ChatColor.AQUA + "[" + ChatColor.GOLD + p.getDisplayName() + ChatColor.AQUA + " <- " + "me" + ChatColor.AQUA + "] " + ChatColor.GRAY + message);
					p.sendMessage(ChatColor.AQUA + "[" + ChatColor.GOLD + "Me" + ChatColor.AQUA + " -> " + target.getDisplayName() + ChatColor.AQUA + "] " + ChatColor.GRAY + message);
				}
			}
		}
		return false;
	}
}
