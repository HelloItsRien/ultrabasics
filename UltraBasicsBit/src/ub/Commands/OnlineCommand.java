package ub.Commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class OnlineCommand implements CommandExecutor{
	public UltraBasics plugin;
	public OnlineCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		} else {
			Player p = (Player) sender;
			if(args.length == 0){
				if(plugin.m.checkPermission(p, "ultrabasics.online", true)){
					p.sendMessage(plugin.prefix + ChatColor.GOLD + "Online players:");
					StringBuilder sb = new StringBuilder();
					for(Player player : Bukkit.getServer().getOnlinePlayers()) {
						sb.append(ChatColor.RESET + player.getName() + ChatColor.GRAY + ", ");
					}
					String playerList = sb.toString();
					Pattern pattern = Pattern.compile(", $");
					Matcher matcher = pattern.matcher(playerList);
					playerList = matcher.replaceAll("");
					p.sendMessage(playerList);
				}
			} else {
				plugin.noRightArgs(p, "/online");
			}
		}
		return false;
	}

}