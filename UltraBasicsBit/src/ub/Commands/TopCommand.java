package ub.Commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class TopCommand implements CommandExecutor{
	public UltraBasics plugin;
	public TopCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length != 0){
			plugin.noRightArgs(p, "top");
			return true;
		}
		if(plugin.m.checkPermission(p, "ultrabasics.top", true)){
			Location l = p.getLocation();
			World w = l.getWorld();
			Block h = w.getHighestBlockAt(l);
			Location t = h.getLocation();
			p.teleport(t);
			p.sendMessage(plugin.prefix + ChatColor.GREEN + "You're now on the top!");
		}
		return false;
	}

}
