package ub.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class FlyCommand implements CommandExecutor {
	public UltraBasics plugin;
	public FlyCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player)sender;
		if(!(sender instanceof Player)){
			return true;
		}
		if (args.length == 0) {
			if(plugin.m.checkPermission(p, "ultrabasics.fly", true)){
				if (p.getAllowFlight()) {
					p.setAllowFlight(false);
					p.sendMessage(ChatColor.YELLOW + "You have disabled fly");
				} else {
					p.setAllowFlight(true);
					p.sendMessage(ChatColor.YELLOW + "You have enabled fly");
				}
			}
		}
		if (args.length == 1) {
			if(plugin.m.checkPermission(p, "ultrabasics.fly.other", true)){
				Player target = Bukkit.getServer().getPlayer(args[0]);
				if (target == null) {
					p.sendMessage(ChatColor.YELLOW + "Invalid player");
				}
				if (target.getAllowFlight()) {
					target.setAllowFlight(false);
					p.sendMessage(ChatColor.YELLOW + "You have taken " + target.getName() + "'s fly mode!");
					target.sendMessage(ChatColor.YELLOW + p.getName() + " has taken your fly mode!");
				} else {
					target.setAllowFlight(true);
					p.sendMessage(ChatColor.YELLOW + "You have given " + target.getName() + " fly mode!");
					target.sendMessage(ChatColor.YELLOW + p.getName() + " has given you fly mode!");
				}
			}
		}
		return false;
	}
}
