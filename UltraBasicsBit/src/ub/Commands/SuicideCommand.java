package ub.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class SuicideCommand implements CommandExecutor {
	public UltraBasics plugin;
	public SuicideCommand(UltraBasics plugin){
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)){
			return true;
		}
		
		Player p = (Player)sender;
		if(cmd.getName().equalsIgnoreCase("suicide")){
			if(plugin.m.checkPermission(p, "ultrabasics.suicide", true)){
				p.setHealth(0.0D);
				p.sendMessage(plugin.prefix
						 + ChatColor.YELLOW + "So, i hate my life");
			}
		}
		return false;	
	}
}
