package ub.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class WarpsCommand implements CommandExecutor{
	public UltraBasics plugin;
	public WarpsCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length != 0){
			plugin.noRightArgs(p, "/Warps");
			return true;
		}
		if(plugin.m.checkPermission(p, "ultrabasics.warps", true)){
			p.sendMessage(plugin.prefix + ChatColor.GOLD + "Warps (" + String.valueOf(plugin.wm.warpAmount()) + "):");
			p.sendMessage(plugin.wm.warpsToString());
		}
		return false;
	}

}
