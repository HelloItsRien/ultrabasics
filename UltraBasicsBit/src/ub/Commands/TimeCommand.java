package ub.Commands;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class TimeCommand implements CommandExecutor{
	public UltraBasics plugin;
	public TimeCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length == 0){
			World w = p.getWorld();
			p.sendMessage(ChatColor.BLUE + "It's now in world '" + w.getName() + "' " + getDayOrNight(w));
		}
		if(args.length == 1){
			World w = p.getWorld();
			if(plugin.m.checkPermission(p, "ultrabasics.time", true)){
				if(args[0].equalsIgnoreCase("day")){
					w.setTime(0L);
					
					return true;
				}
				if(args[0].equalsIgnoreCase("night")){
					w.setTime(12000L);
					
					return true;
				}
				if(isLong(args[0])){
					long l = Long.parseLong(args[0]);
					w.setTime(l);
					
					return true;
				}
			}
		}
		return false;
	}

	public String getDayOrNight(World w){
		if(w.getTime() <= 11000 && w.getTime() >= 0){
			return "day";
		} else {
			return "night";
		}
	}
	
	public boolean isLong(String s) {
	    if (s == null) {
	        return false;
	    }
	    int sz = s.length();
	    for (int i = 0; i < sz; i++) {
	        if (Character.isDigit(s.charAt(i)) == false) {
	            return false;
	        }
	    }
	    return true;
	}
}
