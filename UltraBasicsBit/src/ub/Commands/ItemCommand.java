package ub.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import ub.Main.UltraBasics;

public class ItemCommand implements CommandExecutor{
	public UltraBasics plugin;
	public ItemCommand(UltraBasics plugin){
		this.plugin = plugin;
	}
	@SuppressWarnings({ "unused", "deprecation" })
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length <= 1){
			plugin.noRightArgs(p, "/item <Player> <Amount>");
			return true;
		}
		if(args.length == 2){
			Player t = Bukkit.getPlayer(args[0]);
			if(t == null || !t.isOnline()){
				plugin.playerNotOnline(p, t);
				return true;
			}
			ItemStack i = new ItemStack(Material.getMaterial(args[1]), 64);
			if(i == null){
				p.sendMessage(plugin.prefix + ChatColor.RED + "Material is unknown!");
				return true;
			}
			if(plugin.m.checkPermission(p, "ultrabasics.item." + i.getTypeId(), true)){
				t.getInventory().addItem(i);
			}
		}
		
		if(args.length == 3){
			Player t = Bukkit.getPlayer(args[0]);
			if(t == null || !t.isOnline()){
				plugin.playerNotOnline(p, t);
				return true;
			}
			ItemStack i = new ItemStack(Material.getMaterial(args[1]), Integer.getInteger(args[2]));
			if(i == null){
				p.sendMessage(plugin.prefix + ChatColor.RED + "Material is unknown!");
				return true;
			}
			if(plugin.m.checkPermission(p, "ultrabasics.item." + i.getTypeId(), true)){
				t.getInventory().addItem(i);
			}
		}
		return false;
	}

}
