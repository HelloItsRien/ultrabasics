package ub.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import ub.Main.UltraBasics;

public class CopyItemCommand implements CommandExecutor{
	public UltraBasics plugin;
	public CopyItemCommand(UltraBasics plugin){
		this.plugin = plugin;
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length != 0){
			plugin.noRightArgs(p, "/CopyItem | /Copy");
			return true;
		}
		ItemStack ih = p.getItemInHand();
		if(ih == null){
			p.sendMessage(plugin.prefix + ChatColor.RED + "You don't have anything in your hand!");
			return true;
		}
		ItemStack n = ih.clone();
		if(plugin.m.checkPermission(p, "ultrabasics.copyitem", true)){
			if(p.getInventory().firstEmpty() == -1){
				p.sendMessage(plugin.prefix + ChatColor.RED + "Your inventory is full, please clear it first!");
				return true;
			}
			p.getInventory().addItem(n);
			p.sendMessage(plugin.prefix + ChatColor.GREEN + "You've copied the item in your hand!");
		}
		return false;
	}

}
