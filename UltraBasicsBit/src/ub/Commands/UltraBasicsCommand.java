package ub.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class UltraBasicsCommand implements CommandExecutor {
	public UltraBasics plugin;
	public UltraBasicsCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player)sender;

		if(!(sender instanceof Player)){
			return true;
		}

		if(args.length == 0){
			p.sendMessage(ChatColor.WHITE + "-=-=-=-=-" + ChatColor.DARK_RED + "UltraBasics" + ChatColor.WHITE + "-=-=-=-=-");
			p.sendMessage(ChatColor.BLUE + "This server is using UltraBasics");
			p.sendMessage(ChatColor.BLUE + "Author: ChunkMe");
			p.sendMessage(ChatColor.BLUE + "Author/Maintainer: Shadow48402");
			p.sendMessage(ChatColor.BLUE + "Author: voodootje");
		}

		if(args.length == 1){
			if(args[0].equalsIgnoreCase("reload")){
				plugin.sm.reloadSettings();
				plugin.km.reloadKits();
			}
		}

		return false;
	}
}