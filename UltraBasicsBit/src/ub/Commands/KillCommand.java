package ub.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class KillCommand implements CommandExecutor {
	public UltraBasics plugin;
	public KillCommand(UltraBasics plugin){
		this.plugin = plugin;
	}
	
	//Kill command in development.
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player)sender;
		if (args.length == 0){
			if(plugin.m.checkPermission(p, "ultrabasics.kill", true)){
				p.setHealth(0.0);
				p.sendMessage(ChatColor.YELLOW + "Bye bye, cruel life!");
			}else{
				p.sendMessage(ChatColor.YELLOW + "You don't have the needed permission");
			}
		}
		if(args.length == 1){
			if(plugin.m.checkPermission(p, "ultrabasics.kill.other", true)){
				Player target = Bukkit.getServer().getPlayer(args[0]);
				if (target == null){
					p.sendMessage(plugin.prefix + ChatColor.YELLOW + "Invalid player");
					return true;
				}
				target.setHealth(0.0);
				target.sendMessage(ChatColor.YELLOW + "Bye bye, cruel life!");
				p.sendMessage(plugin.prefix + ChatColor.YELLOW + "You toke the life from " + target.getDisplayName());
			}	
		}
		return false;
	}
}
