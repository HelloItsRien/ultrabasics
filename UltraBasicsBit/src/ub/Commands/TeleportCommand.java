package ub.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class TeleportCommand implements CommandExecutor{
	public UltraBasics plugin;
	public TeleportCommand(UltraBasics plugin){
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		if(args.length == 0){
			Player p = (Player) sender;
			plugin.noRightArgs(p, "/Teleport <Player> (<Player>)");
		}
		if(args.length == 1){
			Player p = (Player) sender;
			Player t = Bukkit.getPlayer(args[0]);
			if(t == null || !t.isOnline()){
				plugin.playerNotOnline(p, t);
				return true;
			}
			if(plugin.disabletp.contains(t)){
				p.sendMessage(plugin.prefix + ChatColor.RED + "That player has disabled his teleporting!");
				return true;
			}
			if(plugin.m.checkPermission(p, "ultrabasics.teleport", true)){
				p.teleport(t);
				p.sendMessage(ChatColor.GREEN + "You're teleported! :)");
				t.sendMessage(ChatColor.AQUA + p.getDisplayName() + " teleported to you!");
			}
		}
		if(args.length == 2){
			Player p = (Player) sender;
			Player t1 = Bukkit.getPlayer(args[0]);
			Player t2 = Bukkit.getPlayer(args[0]);
			if(t1 == null || !t1.isOnline()){
				plugin.playerNotOnline(p, t1);
				return true;
			}
			if(t2 == null || !t2.isOnline()){
				plugin.playerNotOnline(p, t2);
				return true;
			}
			if(plugin.disabletp.contains(t1) || plugin.disabletp.contains(t2)){
				p.sendMessage(plugin.prefix + ChatColor.RED + "That player has disabled his teleporting!");
				return true;
			}
			if(plugin.m.checkPermission(p, "ultrabasics.teleport.other", true)){
				t1.teleport(t2);
				t1.sendMessage(ChatColor.GREEN + "You're teleported! :)");
				t2.sendMessage(ChatColor.AQUA + p.getDisplayName() + " teleported to you!");
			}
		}
		if(args.length > 2){
			Player p = (Player) sender;
			plugin.noRightArgs(p, "/Teleport <Player> (<Player>)");
		}
		return false;
	}

}
