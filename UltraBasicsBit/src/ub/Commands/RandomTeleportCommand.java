package ub.Commands;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class RandomTeleportCommand implements CommandExecutor{
	public UltraBasics plugin;
	public RandomTeleportCommand(UltraBasics plugin){
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length != 0){
			plugin.noRightArgs(p, "/randomteleport | /rt");
			return true;
		}
		if(plugin.m.checkPermission(p, "ultrabasics.randomteleport", true)){
			Random r = new Random();
			World world = p.getWorld();
			int x = (int) (r.nextInt(1000) + 1 * r.nextInt(3) + 1 - r.nextInt(500) + 1);
			int z = (int) (r.nextInt(1000) + 1 * r.nextInt(3) + 1 - r.nextInt(500) + 1);
			Block b = world.getHighestBlockAt(x, z);
			Location l = new Location(world, x, b.getY(), z);
			p.teleport(l);
			p.sendMessage(plugin.prefix + ChatColor.GREEN + "You're now on a random location!");
		}
		return false;
	}

}
