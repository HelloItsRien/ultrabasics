package ub.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class UnfreezeCommand implements CommandExecutor{
	public UltraBasics plugin;
	public UnfreezeCommand(UltraBasics plugin){
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length == 0){
			plugin.noRightArgs(p, "/Unfreeze <Player>");
		}
		else if(args.length == 1){
			Player t = Bukkit.getPlayer(args[0]);
			if(t != null){
				if(plugin.m.checkPermission(p, "ultrabasics.unfreeze", true)){
					if(plugin.frozen.contains(t)){
						plugin.frozen.remove(t);
						t.sendMessage(plugin.prefix + ChatColor.AQUA + "You're not longer frozen!");
						p.sendMessage(plugin.prefix + ChatColor.GREEN + "You've unfrozen " + t.getDisplayName() + "!");
					}
				}
			} else {
				plugin.playerNotOnline(p, t);
			}
		} else {
			plugin.noRightArgs(p, "/Unfreeze <Player>");
		}
		return false;
	}

}
