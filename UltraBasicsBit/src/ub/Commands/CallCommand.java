package ub.Commands;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class CallCommand implements CommandExecutor{
	public HashMap<String, String> teleport = new HashMap<String, String>();
	public UltraBasics plugin;
	public CallCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	/*
	 * From NiSystem: Made by Shadow48402
	 */

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		final Player player = (Player) sender;
		if(args.length == 0){
			player.sendMessage(ChatColor.RED + "Usage: /<command> <player>");
		}
		else if(args.length == 1){
			if(plugin.m.checkPermission(player, "ultrabasics.call", true)){
				final Player targetp = Bukkit.getPlayer(args[0]);
				if(targetp.isOnline()){
					if(!plugin.disabletp.contains(targetp)){
						targetp.sendMessage(plugin.prefix + ChatColor.BLUE + player.getName() + " wants to teleport to you. Type '/call -accept' if you want him to tp to you.");
						player.sendMessage(plugin.prefix + ChatColor.BLUE + "Teleport request sent.");
						teleport.put(targetp.getName(), player.getName());
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
							public void run(){
								teleport.remove(String.valueOf(targetp.getName()));
								teleport.remove(String.valueOf(player.getName()));
								player.sendMessage(plugin.prefix + ChatColor.RED + "You was waiting for to long! The call is now closed.");
							}
						}, 20*20);
					} else {
						player.sendMessage(plugin.prefix + ChatColor.RED + "That player has disabled his teleporting!");
					}
				} else {
					player.sendMessage(plugin.prefix + ChatColor.RED + "That player is not online!");
				}
			}

			if(args[0].equalsIgnoreCase("-accept")){
				if(player.hasPermission("NiSystem.call")){
					if(teleport.get(player.getName()) == null){
						player.sendMessage(plugin.prefix + ChatColor.RED + "That player isn't here.");
					}else{ 
						String target = teleport.get(player.getName());
						player.teleport(Bukkit.getPlayerExact(teleport.get(target)));
						teleport.remove(player);
						teleport.remove(target);
						player.sendMessage(plugin.prefix + ChatColor.GREEN + "Teleport Successful.");
						return true; 
					}
				}
			}
			if(args[0].equalsIgnoreCase("-all")){
				if(plugin.m.checkPermission(player, "ultrabasics.call.all", true)){
					if(Bukkit.getOnlinePlayers().length > 1){
						for(final Player targetp : Bukkit.getOnlinePlayers()){
							targetp.sendMessage(plugin.prefix + ChatColor.BLUE + player.getName() + " wants to teleport to you. Type '/call -accept' if you want him to tp to you.");
							player.sendMessage(plugin.prefix + ChatColor.BLUE + "You've requested everybody for an invite!");
							teleport.put(targetp.getName(), player.getName());
							Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
								public void run(){
									teleport.remove(String.valueOf(targetp.getName()));
									teleport.remove(String.valueOf(player.getName()));
									player.sendMessage(plugin.prefix + ChatColor.RED + "You was waiting for to long! The call is now closed.");
								}
							}, 20*20);
						}
					} else {
						player.sendMessage(plugin.prefix + ChatColor.RED + "You're the only one online!");
					}
				}
			}
		}
		return false;
	}
}