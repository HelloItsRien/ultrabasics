package ub.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_7_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class PingCommand implements CommandExecutor{
	public UltraBasics plugin;
	public PingCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length == 0){
			CraftPlayer cp = (CraftPlayer)p;
			p.sendMessage(plugin.prefix + ChatColor.AQUA + "Your ping: " + ChatColor.GRAY + String.valueOf(cp.getHandle().ping));
		}
		return false;
	}

}
