package ub.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class FeedCommand implements CommandExecutor{
	public UltraBasics plugin;
	public FeedCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		} else {
			Player p = (Player) sender;
			if(args.length == 0){
				if(plugin.m.checkPermission(p, "ultrabasics.feed", true)){
					p.sendMessage(plugin.prefix + ChatColor.GREEN + "Your foodbar is restored!");
					p.setFoodLevel(20);
				}
			}
			else if(args.length == 1){
				Player t = Bukkit.getPlayer(args[0]);
				if(plugin.m.checkPermission(p, "ultrabasics.feed.other", true)){
					if(t != null){
						p.sendMessage(plugin.prefix + ChatColor.GREEN + "You've restored the foodbar of " + ChatColor.DARK_GREEN + t.getDisplayName() + ChatColor.GREEN + "!");
						t.sendMessage(plugin.prefix + ChatColor.GREEN + "Your foodbar is restored by " + ChatColor.DARK_GREEN + p.getDisplayName() + ChatColor.GREEN + "!");
						t.setFoodLevel(20);
					} else {
						plugin.playerNotOnline(p, t);
					}
				}
			} else {
				plugin.noRightArgs(p, "/feed | /feed <Player>");
			}
		}
		return false;
	}

}