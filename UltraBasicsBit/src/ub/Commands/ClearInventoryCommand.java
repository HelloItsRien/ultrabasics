package ub.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

import ub.Main.UltraBasics;

public class ClearInventoryCommand implements CommandExecutor{
	public UltraBasics plugin;
	public ClearInventoryCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		} else {
			Player p = (Player) sender;
			PlayerInventory i = p.getInventory();
			if(args.length == 0){
				if(plugin.m.checkPermission(p, "ultrabasics.ci", true) || plugin.m.checkPermission(p, "ultrabasics.clearinventory", true)){
					i.clear();
				}
			}
			if(args.length == 1){
				Player t = Bukkit.getPlayer(args[0]);
				if(plugin.m.checkPermission(p, "ultrabasics.ci.other", true) || plugin.m.checkPermission(p, "ultrabasics.clearinventory.other", true)){
					if(t != null){
						PlayerInventory inv = t.getInventory();
						inv.clear();
					} else {
						plugin.playerNotOnline(p, t);
					}
				}
			}
		}
		return false;
	}

}