package ub.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class ClearArmorCommand implements CommandExecutor {
	public UltraBasics plugin;
	public ClearArmorCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		} else {
			Player p = (Player) sender;
			if(args.length == 0){
				if(plugin.m.checkPermission(p, "ultrabasics.cleararmor", true) || plugin.m.checkPermission(p, "ultrabasics.ca", true)){
					plugin.m.removeArmor(p);
					p.sendMessage(plugin.prefix + ChatColor.GREEN + "Your armor has been removed!");
				}
			}
			if(args.length == 1){
				Player t = Bukkit.getPlayer(args[0]);
				if(plugin.m.checkPermission(p, "ultrabasics.cleararmor.other", true) || plugin.m.checkPermission(p, "ultrabasics.ca.other", true)){
					if(t != null){
						plugin.m.removeArmor(t);
						t.sendMessage(plugin.prefix + ChatColor.GREEN + "Your armor has been removed!");
					} else {
						plugin.playerNotOnline(p, t);
					}
				}
			}
		}
		return false;
	}

}
