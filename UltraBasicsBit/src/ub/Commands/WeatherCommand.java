package ub.Commands;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class WeatherCommand implements CommandExecutor{
	public UltraBasics plugin;
	public WeatherCommand(UltraBasics plugin){
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(args.length != 0 || args.length != 1 || args.length != 2){
			plugin.noRightArgs(p, "/Weather | /Weather <Weather> | /Weather <Weather> <Duration>");
			p.sendMessage(ChatColor.AQUA + "Weather types: sun, storm");
			return true;
		}
		
		if(args.length == 0){
			World w = p.getWorld();
			if(w.hasStorm()){
				p.sendMessage(plugin.prefix + ChatColor.GOLD + "It's storming now! :o");
			} else {
				p.sendMessage(plugin.prefix + ChatColor.GOLD + "You're lucky it's not storming!");
			}
		}
		
		if(args.length == 1){
			World w = p.getWorld();
			if(args[0].equalsIgnoreCase("sun")){
				w.setStorm(false);
				p.sendMessage(plugin.prefix + ChatColor.AQUA + "You set the weather to sunny");
			}
			if(args[0].equalsIgnoreCase("storm")){
				w.setStorm(false);
				p.sendMessage(plugin.prefix + ChatColor.AQUA + "You set the weather to storm");
			}
		}
		
		if(args.length == 2){
			World w = p.getWorld();
			int d = Integer.valueOf(args[1]);
			if(args[0].equalsIgnoreCase("sun")){
				if(!isNumber(args[1])){
					p.sendMessage(plugin.prefix + ChatColor.RED + "'" + args[1] + "' is not right!");
					return true;
				}
				w.setStorm(false);
				p.sendMessage(plugin.prefix + ChatColor.AQUA + "You set the weather to sunny for " + String.valueOf(d) + " ticks!");
				w.setWeatherDuration(d);
			}
			if(args[0].equalsIgnoreCase("storm")){
				if(!isNumber(args[1])){
					p.sendMessage(plugin.prefix + ChatColor.RED + "'" + args[1] + "' is not right!");
					return true;
				}
				w.setStorm(false);
				p.sendMessage(plugin.prefix + ChatColor.AQUA + "You set the weather to storm for " + String.valueOf(d) + " ticks!");
				w.setWeatherDuration(d);
			}
		}
		
		return false;
	}
	
	public boolean isNumber(String s){
		try{
			Integer.parseInt(s);
			return true;
		}
		catch(Exception e){
			
		}
		return false;
	}

}
