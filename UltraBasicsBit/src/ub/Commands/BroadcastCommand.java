package ub.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class BroadcastCommand implements CommandExecutor{
	public UltraBasics plugin;
	public BroadcastCommand(UltraBasics plugin){
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		} else {
			Player p = (Player) sender;
			if(args.length == 0){
				plugin.noRightArgs(p, "/broadcast <message...>");
			} else {
				if(plugin.m.checkPermission(p, "ultrabasics.broadcast", true)){
					String m = "";
					for (int i = 0; i < args.length; i++) {
						m += args[i] + " ";
					}
					plugin.m.broadcastMessage(m);
				}
			}
		}
		return false;
	}
}