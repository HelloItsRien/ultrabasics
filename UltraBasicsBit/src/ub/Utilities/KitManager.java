package ub.Utilities;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.*;

import ub.Main.UltraBasics;

public class KitManager {
	public UltraBasics plugin;
	public KitManager(UltraBasics plugin){
		this.plugin = plugin;
	}

	FileConfiguration Kits;
	File kfile;

	//Kits.YML:
	public void setupKits(){
		this.kfile = new File(plugin.getDataFolder(), "Kits.yml");

		plugin.logger.info("Checking or Kits.yml exists");
		if (!this.kfile.exists()) {
			try {
				plugin.logger.info("Kits.yml does not exists");
				plugin.logger.info("Creating Kits.yml...");
				this.kfile.createNewFile();
				plugin.logger.info("Created Kits.yml");
			}
			catch (IOException e) {
				plugin.logger.info("Could not create Kits.yml!");
			}
		} else {
			plugin.logger.info("Kits.yml exists");
		}
		this.Kits = YamlConfiguration.loadConfiguration(this.kfile);
	}

	public FileConfiguration getKits() {
		return this.Kits;
	}

	public void saveKits() {
		try {
			plugin.logger.info("Saving Kits.yml...");
			this.Kits.save(this.kfile);
			plugin.logger.info("Saved Kits.yml");
		}
		catch (IOException e) {
			plugin.logger.info("Could not save Kits.yml!");
			e.printStackTrace();
		}
	}

	public void reloadKits() {
		try{
			plugin.logger.info("Reloading Kits.yml...");
			this.Kits = YamlConfiguration.loadConfiguration(this.kfile);
			plugin.logger.info("Reloaded Kits.yml");
		}
		catch(Exception e){
			plugin.logger.info("Could not load Kits.yml");
		}
	}
	
	public List<String> getKitList(){
		return getKits().getStringList("Kits");
	}
	
	public void addKit(String n){
		getKitList().add(n);
	}
	
	@SuppressWarnings("deprecation")
	public ItemStack parseString(String itemId) {
	    String[] parts = itemId.split(":");
	    int matId = Integer.parseInt(parts[0]);
	    if (parts.length == 2) {
	        short data = Short.parseShort(parts[1]);
	        return new ItemStack(Material.getMaterial(matId), 1, data);
	    }
	    return new ItemStack(Material.getMaterial(matId));
	}

}