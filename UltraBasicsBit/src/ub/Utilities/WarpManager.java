package ub.Utilities;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import ub.Main.UltraBasics;

public class WarpManager {
	public UltraBasics plugin;
	public WarpManager(UltraBasics plugin){
		this.plugin = plugin;
	}

	public void createWarpMap() throws IOException{
		File map = new File("Warps/");
		if(!map.exists()){
			map.mkdirs();
			File list = new File("Warps/list.yml");
			list.mkdir();
		} else {
			plugin.logger.info("The warp map does already exist! :)");
		}
	}

	public void createWarp(String wname, Location l, Player owner){
		File wfile = new File("Warps/" + wname + ".yml");

		if(wname != "list"){
			if(!wfile.exists()){
				wfile.mkdir();
				FileConfiguration f = YamlConfiguration.loadConfiguration(wfile);
				f.set("Name", wname);
				f.set("Owner", owner.getName());
				f.set("World", l.getWorld().getName());
				f.set("X", l.getX());
				f.set("Y", l.getY());
				f.set("Z", l.getZ());
				f.set("Pitch", l.getPitch());
				f.set("Yaw", l.getYaw());

				warpList().add(wname);
			} else {
				owner.sendMessage(plugin.prefix + ChatColor.RED + "That warp does already exist!");
			}
		} else {
			owner.sendMessage(plugin.prefix + ChatColor.RED + "That name is not valid!");
		}
	}

	public Location getWarp(String wname, Player sender, boolean m){
		File wfile = new File("Warps/" + wname + ".yml");
		if(wfile.exists()){
			FileConfiguration f = YamlConfiguration.loadConfiguration(wfile);
			return new Location(Bukkit.getServer().getWorld(f.getString("World")), f.getDouble("X"), f.getDouble("Y"), f.getDouble("Z"), (float) f.get("Pitch"), (float) f.get("Yaw"));
		} else {
			if(m == true){
				sender.sendMessage(plugin.prefix + ChatColor.RED + "That warp doesn't exist!");
			}
		}
		return null;
	}

	public List<String> warpList(){
		File f = new File("Warps/list.yml");
		FileConfiguration c = YamlConfiguration.loadConfiguration(f);

		return c.getStringList("Warps");
	}

	public int warpAmount(){
		return warpList().size();
	}

	public String warpsToString(){
		StringBuilder b = new StringBuilder();
		for(String s : warpList()){
			b.append(s).append(ChatColor.GRAY + ", " + ChatColor.GREEN);
		}
		return b.toString();
	}

	public boolean isExistingWarp(String wname){
		if(warpList().contains(wname)){
			return true;
		} else {
			return false;
		}
	}

}
