package ub.Utilities;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import ub.Main.UltraBasics;

public class SettingsManager {
	public UltraBasics plugin;
	public SettingsManager(UltraBasics plugin){
		this.plugin = plugin;
	}
	
	private FileConfiguration Settings;
	private File sfile;

	//Settings.YML:
	public void setupSettings(){
		this.sfile = new File(plugin.getDataFolder(), "Settings.yml");

		plugin.logger.info("Checking or Settings.yml exists");
		if (!this.sfile.exists()) {
			try {
				plugin.logger.info("Settings.yml does not exists");
				plugin.logger.info("Creating Settings.yml...");
				this.sfile.createNewFile();
				plugin.logger.info("Created Settings.yml");
				
				
			}
			catch (IOException e) {
				plugin.logger.info("Could not create Settings.yml!");
			}
			
			try{
				plugin.logger.info("Settings Defaults...");
				this.Settings.addDefault("BlockAdminKilling", false);
				this.Settings.addDefault("BlockAdminDamage", false);
				plugin.logger.info("Defaults set succesful!");
			}
			catch(Exception e){
				plugin.logger.info("Could not set the defaults!");
			}
		} else {
			plugin.logger.info("Settings.yml exists");
		}
		this.Settings = YamlConfiguration.loadConfiguration(this.sfile);
	}

	public FileConfiguration getSettings() {
		return this.Settings;
	}

	public void saveSettings() {
		try {
			plugin.logger.info("Saving Settings.yml...");
			this.Settings.save(this.sfile);
			plugin.logger.info("Saved Settings.yml");
		}
		catch (IOException e) {
			plugin.logger.info("Could not save Settings.yml!");
			e.printStackTrace();
		}
	}

	public void reloadSettings() {
		try{
			plugin.logger.info("Reloading Settings.yml...");
			this.Settings = YamlConfiguration.loadConfiguration(this.sfile);
			plugin.logger.info("Reloaded Settings.yml");
		}
		catch(Exception e){
			plugin.logger.info("Could not load Settings.yml");
		}
	}

}