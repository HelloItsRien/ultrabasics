package ub.Utilities;

import java.util.Arrays;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.Permission;

import ub.Main.UltraBasics;

public class Methodes {
	public UltraBasics plugin;
	public Methodes(UltraBasics plugin){
		this.plugin = plugin;
	}

	//Strings:
	public String getItemName(Material m){
		return m.toString();
	}


	/************************************************************
	/*
	 * m: Message
	 */
	public void broadcastMessage(String[] m){
		for(Player o : Bukkit.getOnlinePlayers()){
			o.sendMessage(plugin.broadcastprefix + ChatColor.GRAY + m);
		}
	}
	public void broadcastMessage(String m){
		for(Player o : Bukkit.getOnlinePlayers()){
			o.sendMessage(plugin.broadcastprefix + ChatColor.GRAY + m);

		}
	}
	/************************************************************/

	/************************************************************
	/*
	 * p: Player
	 * pe: Permission
	 */
	public boolean checkPermission(Player p, String pe, boolean m){
		if(p.hasPermission(pe)){
			return true;
		} else {
			if(m == true){
				plugin.noPerms(p);
			}
			return false;
		}
	}
	public boolean checkPermission(Player p, Permission pe, boolean m){
		if(p.hasPermission(pe)){
			return true;
		} else {
			if(m == true){
				plugin.noPerms(p);
			}
			return false;
		}
	}
	/************************************************************/

	/************************************************************
	/*
	 * p: Player
	 * 
	 */
	public String getName(Player p){
		if(p.getDisplayName() != null){
			return p.getDisplayName();
		} else {
			return p.getName();
		}
	}
	/************************************************************/

	/***************************************************************************************************************************
	/*
	 * m: Material
	 * dname: Displayname
	 * 
	 * a: Amount
	 * 
	 * lore: Lore
	 * lore2: Second rule of the Lore
	 */
	public ItemStack createItem(Material m, String dname){
		ItemStack is = new ItemStack(m);
		ItemMeta im = is.getItemMeta();
		if(dname == null){
			im.setDisplayName(m.name());
		} else {
			im.setDisplayName(dname);
		}
		is.setItemMeta(im);

		return is;
	}
	public ItemStack createItem(Material m, int a, String dname){
		if(a == 0){
			plugin.logger.warning("Item: " + dname + " ERROR (amount = null)! Report it to the author at the plugin page.");
		}
		ItemStack is = new ItemStack(m, a);
		ItemMeta im = is.getItemMeta();
		if(dname == null){
			im.setDisplayName(m.name());
		} else {
			im.setDisplayName(dname);
		}
		is.setItemMeta(im);

		return is;
	}
	public ItemStack createItem(Material m, int a, String dname, String lore){
		if(a == 0){
			plugin.logger.warning("Item: " + dname + " ERROR (amount = null)! Report it to the author at the plugin page.");
		}
		ItemStack is = new ItemStack(m, a);
		ItemMeta im = is.getItemMeta();
		if(dname == null){
			im.setDisplayName(m.name());
		} else {
			im.setDisplayName(dname);
		}
		im.setLore(Arrays.asList(lore));
		is.setItemMeta(im);

		return is;
	}
	public ItemStack createItem(Material m, int a, String dname, String lore, String lore2){
		if(a == 0){
			plugin.logger.warning("Item: " + dname + " ERROR (amount = null)! Report it to the author at the plugin page.");
		}
		ItemStack is = new ItemStack(m, a);
		ItemMeta im = is.getItemMeta();
		if(dname == null){
			im.setDisplayName(m.name());
		} else {
			im.setDisplayName(dname);
		}
		im.setLore(Arrays.asList(lore, lore2));
		is.setItemMeta(im);

		return is;
	}
	public ItemStack createItem(Material m, int a, String dname, String lore, String lore2, String lore3){
		if(a == 0){
			plugin.logger.warning("Item: " + dname + " ERROR (amount = null)! Report it to the author at the plugin page.");
		}
		ItemStack is = new ItemStack(m, a);
		ItemMeta im = is.getItemMeta();
		if(dname == null){
			im.setDisplayName(m.name());
		} else {
			im.setDisplayName(dname);
		}
		im.setLore(Arrays.asList(lore, lore2, lore3));
		is.setItemMeta(im);

		return is;
	}
	/***************************************************************************************************************************/

	/****************************************************************************
	 *                            Some booleans                                 *
	 ****************************************************************************/
	/*
	 * p: Player
	 * 
	 * (Not used, need to test in next version)
	 */
	public boolean isAdmin(Player p){
		if(checkPermission(p, "ultrabasics.admin", false)){
			return true;
		}
		else if(p.isOp()){
			return true;
		} else {
			return false;
		}
	}

	public boolean setAfk(Player p){
		if(plugin.afk.contains(p)){
			p.sendMessage(plugin.prefix + ChatColor.RED + "You're not longer AFK!");
			broadcastMessage(plugin.prefix + ChatColor.GOLD + p.getDisplayName() + " is not longer AFK!");
			plugin.afk.remove(p);
		} else {
			p.sendMessage(plugin.prefix + ChatColor.RED + "You're now AFK!");
			broadcastMessage(plugin.prefix + ChatColor.GOLD + p.getDisplayName() + " is now AFK!");
			plugin.afk.add(p);
		}
		return false;
	}

	/****************************************************************************/


	/****************************************************************************
	 *                              Some voids                                  *
	 ****************************************************************************/
	public void setHat(Player p, ItemStack i){
		PlayerInventory inv = p.getInventory();
		if(i != null){
			try{
				inv.setHelmet(i);
				p.sendMessage(plugin.prefix + ChatColor.GREEN + "You've a new hat! " + ChatColor.GRAY + "(" + ChatColor.AQUA + getItemName(i.getType()) + ChatColor.GRAY + ")");
			}
			catch(Exception e){
				p.sendMessage(plugin.prefix + ChatColor.RED + "That item is not valid for placing on your hat! :o");
				e.printStackTrace();
			}
		}
	}

	public void removeArmor(Player p){
		PlayerInventory i = p.getInventory();
		i.setHelmet(null);
		i.setChestplate(null);
		i.setLeggings(null);
		i.setBoots(null);
	}



	/****************************************************************************/

	/****************************************************************************
	 *                             Some Integers                                *
	 ****************************************************************************/
	public int getPing(Player p){
		CraftPlayer cp = (CraftPlayer) p;

		return cp.getHandle().ping;
	}

	public int getRandom(int max){
		Random r = new Random();
		int ri = r.nextInt(max) + 1;

		return ri;
	}

	/****************************************************************************/
}