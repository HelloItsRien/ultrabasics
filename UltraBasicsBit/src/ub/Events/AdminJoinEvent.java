package ub.Events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import ub.Main.UltraBasics;

public class AdminJoinEvent extends Event implements Cancellable {
	public UltraBasics plugin;
	
	private boolean cancelled;
	private Player p;

	public AdminJoinEvent(UltraBasics plugin, Player p) {
		this.plugin = plugin;
		this.p = p;
	}

	public Player getPlayer(){
		return p;
	}
	
	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean bln) {
		this.cancelled = bln;
	}

	@Override
	public HandlerList getHandlers() {
		return null;
	}

}