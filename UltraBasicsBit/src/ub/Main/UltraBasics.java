package ub.Main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

import ub.Commands.BroadcastCommand;
import ub.Commands.CallCommand;
import ub.Commands.ClearArmorCommand;
import ub.Commands.ClearInventoryCommand;
import ub.Commands.CreativeCommand;
import ub.Commands.FeedCommand;
import ub.Commands.FlyCommand;
import ub.Commands.FreezeCommand;
import ub.Commands.GamemodeCommand;
import ub.Commands.HatCommand;
import ub.Commands.HealCommand;
import ub.Commands.ItemCommand;
import ub.Commands.KillCommand;
import ub.Commands.MessageCommand;
import ub.Commands.NickCommand;
import ub.Commands.OnlineCommand;
import ub.Commands.PingCommand;
import ub.Commands.RandomTeleportCommand;
import ub.Commands.RepairCommand;
import ub.Commands.SetWarpCommand;
import ub.Commands.SuicideCommand;
import ub.Commands.SurvivalCommand;
import ub.Commands.TeleportCommand;
import ub.Commands.ThrowCommand;
import ub.Commands.TopCommand;
import ub.Commands.UltraBasicsCommand;
import ub.Commands.UnfreezeCommand;
import ub.Commands.WarpCommand;
import ub.Commands.WarpsCommand;
import ub.Economy.BalanceCommand;
import ub.Economy.EcoConfig;
import ub.Economy.EcoManagement;
import ub.Economy.JoinSetup;
import ub.Economy.UconomyCommand;
import ub.Listeners.AdminJoinListener;
import ub.Listeners.DamageListener;
import ub.Listeners.DeathListener;
import ub.Listeners.JoinListener;
import ub.Listeners.QuitListener;
import ub.Utilities.KitManager;
import ub.Utilities.Methodes;
import ub.Utilities.SettingsManager;
import ub.Utilities.WarpManager;

public class UltraBasics extends JavaPlugin{
	public Logger logger = Logger.getLogger("Minecraft");
	
	/*******************************************
	 *                Strings                  *
	 *******************************************/
	public String prefix = ChatColor.GRAY + "[" + ChatColor.AQUA + "UltraBasics" + ChatColor.GRAY + "] ";
	public String broadcastprefix = ChatColor.WHITE + "[" + ChatColor.ITALIC + "BroadCast" + ChatColor.GRAY + "] ";
	/*******************************************/
	
	/*******************************************
	 *            Error Messages               *
	 *******************************************/
	public void noPerms(Player p){
		p.sendMessage(prefix + ChatColor.RED + "You're not allowed to do this!");
	}
	public void noRightArgs(Player p, String r){
		p.sendMessage(prefix + ChatColor.RED + "You're not using the right arguments!" + ChatColor.GRAY + " (" + ChatColor.AQUA + r + ChatColor.GRAY + ")");
	}
	public void playerNotOnline(Player p, Player t){
		p.sendMessage(prefix + ChatColor.RED + "That player is not online, or it's not the right name! (" + ChatColor.DARK_RED + t.getName() + ChatColor.RED + ")");
	}
	/*******************************************/
	
	/*******************************************
	 *          Hashmaps / ArrayLists          *
	 *******************************************/
	public ArrayList<Player> frozen = new ArrayList<Player>();
	public ArrayList<Player> afk = new ArrayList<Player>();
	public ArrayList<Player> disabletp = new ArrayList<Player>();
	
	public HashMap<Player, String> status = new HashMap<Player, String>();
	/*******************************************/
	
	/*******************************************
	 *               Listeners                 *
	 *******************************************/
	public AdminJoinListener ajl = new AdminJoinListener(this);
	public DamageListener dal = new DamageListener(this);
	public DeathListener dl = new DeathListener(this);
	public JoinListener jl = new JoinListener(this);
	public QuitListener ql = new QuitListener(this);
	/*******************************************/
	
	/*******************************************
	 *                Commands                 *
	 *******************************************/
	public BroadcastCommand bc = new BroadcastCommand(this);
	public CallCommand calc = new CallCommand(this);
	public ClearArmorCommand cac = new ClearArmorCommand(this);
	public ClearInventoryCommand cic = new ClearInventoryCommand(this);
	public CreativeCommand cc = new CreativeCommand(this);
	public FeedCommand fec = new FeedCommand(this);
	public FlyCommand flc = new FlyCommand(this);
	public FreezeCommand frc = new FreezeCommand(this);
	public GamemodeCommand gac = new GamemodeCommand(this);
	public HatCommand hac = new HatCommand(this);
	public UltraBasicsCommand ub = new UltraBasicsCommand(this);
	public HealCommand hc = new HealCommand(this);
	public ItemCommand ic = new ItemCommand(this);
	public KillCommand kc = new KillCommand(this);
	public MessageCommand mc = new MessageCommand(this);
	public NickCommand nc = new NickCommand(this);
	public OnlineCommand oc = new OnlineCommand(this);
	public PingCommand pc = new PingCommand(this);
	public RandomTeleportCommand rtc = new RandomTeleportCommand(this);
	public RepairCommand rc = new RepairCommand(this);
	public SetWarpCommand swc = new SetWarpCommand(this);
	public SuicideCommand sc = new SuicideCommand(this);
	public SurvivalCommand surc = new SurvivalCommand(this);
	public TeleportCommand tpc = new TeleportCommand(this);
	public ThrowCommand thc = new ThrowCommand(this);
	public TopCommand tc = new TopCommand(this);
	public UltraBasicsCommand ubc = new UltraBasicsCommand(this);
	public WarpCommand wpc = new WarpCommand(this);
	public WarpsCommand wpsc = new WarpsCommand(this);
	public UnfreezeCommand ufc = new UnfreezeCommand(this);
	/*******************************************/
	
	/*******************************************
	 *                 uConomy                 *
	 *******************************************/
	public BalanceCommand ubalc = new BalanceCommand(this); //CommandExecutor
	public UconomyCommand uconc = new UconomyCommand(this); //CommandExecutor
	
	public JoinSetup jsl = new JoinSetup(this); //Listener
	
	public EcoConfig econ = new EcoConfig(this); //Config
	public EcoManagement em = new EcoManagement(this); //Management
	
	/*******************************************/
	
	/*******************************************
	 *                Utilities                *
	 *******************************************/
	public Methodes m = new Methodes(this);
	public SettingsManager sm = new SettingsManager(this);
	public KitManager km = new KitManager(this);
	public WarpManager wm = new WarpManager(this);
	/*******************************************/
	
	/*******************************************
	 *               Scoreboard                *
	 *******************************************/
	public Team dev;
	public ScoreboardManager manager;
	public Scoreboard board;
	/*******************************************/
	
	public void onEnable(){
		//Registering events:
		Bukkit.getServer().getPluginManager().registerEvents(this.ajl, this); //Niels
		Bukkit.getServer().getPluginManager().registerEvents(this.dal, this); //Niels
		Bukkit.getServer().getPluginManager().registerEvents(this.dl, this); //Niels
		Bukkit.getServer().getPluginManager().registerEvents(this.jl, this); //Niels
		Bukkit.getServer().getPluginManager().registerEvents(this.ql, this); //Niels
		Bukkit.getServer().getPluginManager().registerEvents(jsl, this); //Rien
		
		//Registering commands:
		getCommand("broadcast").setExecutor(bc); //Niels
		getCommand("bc").setExecutor(bc); //Niels
		getCommand("call").setExecutor(calc); //Niels
		getCommand("balance").setExecutor(ubalc); //Rien
		getCommand("bal").setExecutor(ubalc); //Rien
		getCommand("money").setExecutor(ubalc); //Rien
		getCommand("ucon").setExecutor(uconc); //Rien
		getCommand("cleararmor").setExecutor(cac); //Niels
		getCommand("ca").setExecutor(cac); //Niels
		getCommand("clearinventory").setExecutor(cic); //Niels
		getCommand("ci").setExecutor(cic); //Niels
		getCommand("creative").setExecutor(cc); //Niels
		getCommand("c").setExecutor(cc); //Niels
		getCommand("feed").setExecutor(fec); //Niels
		getCommand("fly").setExecutor(flc); //Niels
		getCommand("freeze").setExecutor(frc); //Niels
		getCommand("gamemode").setExecutor(gac); //Rien
		getCommand("gm").setExecutor(gac); //Rien
		getCommand("msg").setExecutor(mc); //Rien
		getCommand("hat").setExecutor(hac); // Niels
		getCommand("heal").setExecutor(hc); //Niels
		getCommand("item").setExecutor(ic); //Niels
		getCommand("i").setExecutor(ic); //Niels
		getCommand("ultrabasics").setExecutor(ub);//Rien
		getCommand("kill").setExecutor(kc); //Niels
		getCommand("msg").setExecutor(mc); //Rien
		getCommand("nick").setExecutor(nc); //Niels
		getCommand("online").setExecutor(oc); //Niels
		getCommand("ping").setExecutor(pc); //Niels
		getCommand("randomteleport").setExecutor(rtc); //Niels
		getCommand("repair").setExecutor(rc); //Niels
		getCommand("setwarp").setExecutor(swc); //Niels
		getCommand("rt").setExecutor(rtc); //Niels
		getCommand("suicide").setExecutor(sc); //Rien
		getCommand("survival").setExecutor(surc); //Niels
		getCommand("s").setExecutor(surc); //Niels
		getCommand("teleport").setExecutor(tpc); //Niels
		getCommand("tp").setExecutor(tpc); //Niels
		getCommand("throw").setExecutor(thc); //Niels
		getCommand("top").setExecutor(tc); //Niels
		getCommand("ultrabasics").setExecutor(ubc); //Niels | Rien
		getCommand("ub").setExecutor(ubc); //Niels | Rien
		getCommand("warp").setExecutor(wpc); //Niels
		getCommand("warps").setExecutor(wpsc); //Niels
		getCommand("unfreeze").setExecutor(ufc); //Niels
		
		//Settings.yml
		sm.setupSettings();
		
		//Kits.yml
		km.setupKits();
		
		//Scoreboard:
		this.manager = Bukkit.getScoreboardManager();
		this.board = this.manager.getNewScoreboard();
		this.dev = this.board.registerNewTeam("dev");
		this.dev.setPrefix("�f[�5Developer�f]�b ");
	}
}