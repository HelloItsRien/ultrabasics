package ub.Economy;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import ub.Main.UltraBasics;

public class JoinSetup implements Listener {
	public UltraBasics plugin;
	public JoinSetup(UltraBasics plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void createAccount(PlayerJoinEvent e){
		if(EcoManagement.hasAccount(e.getPlayer().getName())) return;
		EcoManagement.setBalance(e.getPlayer().getName(), 200D);
	}
}
