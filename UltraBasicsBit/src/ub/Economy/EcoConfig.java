package ub.Economy;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import ub.Economy.EcoManagement;
import ub.Main.UltraBasics;

public class EcoConfig {
	private UltraBasics plugin;
	public EcoConfig(UltraBasics plugin){
		this.plugin = plugin;
	}
	
	public void setupEconomyFile(){
		File f = new File("Uconomy.yml");
		FileConfiguration fc = YamlConfiguration.loadConfiguration(f);
		if(!f.exists()){
			f.mkdir();
		}
	}
	
	public void saveBalance(){
		for(String p : EcoManagement.getBalanceMap().keySet()){
			plugin.getConfig().set("balance." +p, EcoManagement.getBalanceMap().get(p));
		}
		plugin.saveConfig();
	}
	
	public void loadBalance(){
		if(plugin.getConfig().contains("balance")) return;
		for(String p : plugin.getConfig().getConfigurationSection("balance").getKeys(false)){
			EcoManagement.setBalance(p, plugin.getConfig().getDouble("balance." + p));
		}
	}
	
}
