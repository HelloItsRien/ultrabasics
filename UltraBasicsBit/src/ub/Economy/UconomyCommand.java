package ub.Economy;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import ub.Main.UltraBasics;

public class UconomyCommand implements CommandExecutor{
	public UltraBasics plugin;
	public UconomyCommand(UltraBasics plugin){
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String s, String[] args){
		if(args.length != 3){
			cs.sendMessage(ChatColor.RED + "Argumented error");
			cs.sendMessage(ChatColor.RED + "Right usage: /ucon <add/remove/set> <player> <amount>");
			return true;
		}
		if(args[0].equalsIgnoreCase("add")){
			if(!EcoManagement.hasAccount(args[1])){
				cs.sendMessage(ChatColor.RED + "You don't have a account!");
				return true;
			}
			double amount = 0;
			try
			{
				amount = Double.parseDouble(args[2]);
			}catch  (Exception e){

				cs.sendMessage(ChatColor.RED + "You don't have put in a number!");
				return true;
			}

			EcoManagement.setBalance(args[1], EcoManagement.getBalance(args[1]) + amount);
		}
		if(args[0].equalsIgnoreCase("remove")){
			if(!EcoManagement.hasAccount(args[1])){
				cs.sendMessage(ChatColor.RED + "You don't have a account!");
				return true;
			}
			double amount = 0;
			try
			{
				amount = Double.parseDouble(args[2]);
			}catch  (Exception e){

				cs.sendMessage(ChatColor.RED + "You don't have put in a number!");
				return true;
			}

			EcoManagement.setBalance(args[1], EcoManagement.getBalance(args[1]) - amount);
		}else if(args[0].equalsIgnoreCase("set")){
			if(!EcoManagement.hasAccount(args[1])){
				cs.sendMessage(ChatColor.RED + "You don't have a account!");
				return true;
			}
			double amount = 0;
			try
			{
				amount = Double.parseDouble(args[2]);
			}catch  (Exception e){

				cs.sendMessage(ChatColor.RED + "You don't have put in a number!");
				return true;
			}

			EcoManagement.setBalance(args[1], amount);
		}
		return true;
	}
}
