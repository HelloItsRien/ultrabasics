package ub.Economy;

import java.util.HashMap;

import ub.Main.UltraBasics;

public class EcoManagement {
	private UltraBasics plugin;
	public EcoManagement(UltraBasics plugin){
		this.plugin = plugin;
	}
	
	public static HashMap<String, Double> bal = new HashMap<>(); // (UUID PLAYER, Balance)

	public static void setBalance(String player, double amount){
		bal.put(player, amount);
	}
	public static Double getBalance(String player){
		return bal.get(player);
	}

	public static boolean hasAccount(String player){
		return bal.containsKey(player);
	}
	public static HashMap<String, Double> getBalanceMap() {
		return bal;
	}
	public static HashMap<String, Double> getMoney() { //Handy trick for developers, ALIAS for getBalanceMap
		return bal;
	}
}
