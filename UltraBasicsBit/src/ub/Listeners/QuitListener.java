package ub.Listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import ub.Main.UltraBasics;

public class QuitListener implements Listener{
	public UltraBasics plugin;
	public QuitListener(UltraBasics plugin){
		this.plugin = plugin;
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		//AFK
		if(plugin.afk.contains(p)){
			plugin.m.setAfk(p);
		}
	}
}
