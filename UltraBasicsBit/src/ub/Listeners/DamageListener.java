package ub.Listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import ub.Main.UltraBasics;

public class DamageListener implements Listener {
	public UltraBasics plugin;
	public DamageListener(UltraBasics plugin){
		this.plugin = plugin;
	} 

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e){
		if(e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
			
			//BlockAdminDamage
			if(plugin.sm.getSettings().getBoolean("BlockAdminDamage") == true){
				if(p.isOp() || plugin.m.checkPermission(p, "ultrabasics.admin", false)){
					if(e.getDamager() instanceof Player){
						Player t = (Player) e.getDamager();
						t.sendMessage(ChatColor.RED + "You're not allowed to attack an Admin!");
					}
					e.setCancelled(true);
				}
			}
		}
	}

}
