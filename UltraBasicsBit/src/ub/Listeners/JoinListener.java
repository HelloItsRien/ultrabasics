package ub.Listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import ub.Events.AdminJoinEvent;
import ub.Main.UltraBasics;

public class JoinListener implements Listener{
	public UltraBasics plugin;
	public JoinListener(UltraBasics plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		
       
		//AdminJoinEvent calling:
		AdminJoinEvent event = new AdminJoinEvent(plugin, p);
		plugin.getServer().getPluginManager().callEvent(event);
       if (event.isCancelled()) {
           return;
       }
       
       //Developer prefix: 
       //Pls add je ign Rien/Mex
       if(p.getName().equalsIgnoreCase("koekjedeeg3")){
    	   plugin.dev.addPlayer(Bukkit.getOfflinePlayer("koekjedeeg3"));
       }
       if(p.getName().equalsIgnoreCase("voodootje0")){
    	   plugin.dev.addPlayer(Bukkit.getOfflinePlayer("voodootje0"));
       }
       if(p.getName().equalsIgnoreCase("ChunkMe")){
    	   plugin.dev.addPlayer(Bukkit.getOfflinePlayer("ChunkMe"));
       }
	}

}