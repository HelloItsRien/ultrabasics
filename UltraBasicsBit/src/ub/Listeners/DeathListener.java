package ub.Listeners;

import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_7_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import ub.Main.UltraBasics;

public class DeathListener implements Listener{
	public UltraBasics plugin;
	public DeathListener(UltraBasics plugin){
		this.plugin = plugin;
	}

	@EventHandler
	public void onFalseDeath(EntityDamageByEntityEvent e){
		if(e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();

			if(plugin.sm.getSettings().getBoolean("BlockAdminKilling") == true){
				if(p.isOp() || plugin.m.checkPermission(p, "ultrabasics.admin", false)){
					if(((CraftPlayer)p).getHandle().getHealth() == 0.0F){
						p.setHealth(20.0D);
						p.sendMessage(plugin.prefix + ChatColor.GREEN + "You can't be killed because you're an admin!");
					}
				}
			}
		}
	}

}