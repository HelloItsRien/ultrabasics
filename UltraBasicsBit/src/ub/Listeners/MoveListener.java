package ub.Listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import ub.Main.UltraBasics;

public class MoveListener implements Listener{
	public UltraBasics plugin;
	public MoveListener(UltraBasics plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		//Freeze:
		if(plugin.frozen.contains(p)){
			p.teleport(e.getFrom());
		}
		
		//AFK:
		if(plugin.afk.contains(p)){
			plugin.m.setAfk(p);
		}
	}


}
